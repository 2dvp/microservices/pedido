package com.fiap.pedido;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PedidoService {
    @Autowired
    PedidoRepository pedidoRepository;

    public List<Pedido> getAllPedido() {
        List<Pedido> pedidos = new ArrayList<Pedido>();
        pedidoRepository.findAll().forEach(pedido -> pedidos.add(pedido));
        return pedidos;
    }

    public Pedido getPedidoById(int id) {
        return pedidoRepository.findById(id).get();
    }

    public void saveOrUpdate(Pedido pedido) {
        pedidoRepository.save(pedido);
    }



    public void delete(int id) {
        pedidoRepository.deleteById(id);
    }
}