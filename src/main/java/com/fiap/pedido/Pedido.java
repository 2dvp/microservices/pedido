package com.fiap.pedido;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PEDIDO")
public class Pedido {
    @Id
    @GeneratedValue
    public int id;
    public String order_status;
    public int product_id; 
    public int user_id; 
    public Integer getId() {
       return id;
   }  
}