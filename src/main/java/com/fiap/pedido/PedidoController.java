package com.fiap.pedido;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PedidoController {

    @Autowired
    PedidoService pedidoService;

    @GetMapping("/pedido")
    private List<Pedido> getAllPedido() {
        return pedidoService.getAllPedido();
    }

    @GetMapping("/pedido/{id}")
    private Pedido getPedido(@PathVariable("id") int id) {
        return pedidoService.getPedidoById(id);
    }

    @DeleteMapping("/pedido/{id}")
    private void deletePedido(@PathVariable("id") int id) {
      pedidoService.delete(id);
    }

    @PostMapping("/pedido")
    private int saveSuporte(@RequestBody Pedido pedido) {
    pedidoService.saveOrUpdate(pedido);
        return pedido.getId();
    }
}